clear 
close all

visualization=1;

con = ConditionsContainer;


con.setPositions(0,20);
con.setTimes(0,10);

conditions = con.getConditions();

in = Interpolator;

in.findCoeffs(conditions);
coeffs = in.getCoefficients();



eval = Evaluator;

frequency = 200; % frequency in Hz

eval.evaluate(coeffs,frequency,conditions);

 trajectory = eval.getTrajectory();
 
 assert(numel(trajectory)==8004)
 
 
 if visualization == 1
 figure('name','evaluator visualisation')
 
 subplot(3,1,1)
 hold on 
 grid on 
 plot(trajectory(1,:),trajectory(2,:))
 ylabel('[]')
 xlabel('time [s]')
 title('position')
 
 
 subplot(3,1,2)
 hold on 
 grid on 
 plot(trajectory(1,:),trajectory(3,:))
 ylabel('[/s]')
 xlabel('time [s]')
  title('velocity')
 
 subplot(3,1,3)
 hold on 
 grid on 
  plot(trajectory(1,:),trajectory(4,:))
 ylabel('[/s^2]')
 xlabel('time [s]')
  title('acceleration')
 end
 
 



 
