close all

visualization=1

conditionsArray(1)=ConditionsContainer;
conditionsArray(2)=ConditionsContainer;
conditionsArray(3)=ConditionsContainer;


conditionsArray(1).setPositions(0,10);
conditionsArray(1).setVelocities(0,0);
conditionsArray(1).setAccelerations(0,0);
conditionsArray(1).setTimes(0,1);

conditionsArray(2).setPositions(10,5);
conditionsArray(2).setVelocities(0,0);
conditionsArray(2).setAccelerations(0,0);
conditionsArray(2).setTimes(1,2);

conditionsArray(3).setPositions(5,0);
conditionsArray(3).setVelocities(0,0);
conditionsArray(3).setAccelerations(0,0);
conditionsArray(3).setTimes(2,3);

assembler = TrajectoryAssembler(conditionsArray);
assembler.assemble(200);

r=assembler.trajectory;


 if visualization == 1
 figure('name','evaluator visualisation')
 
 subplot(3,1,1)
 hold on 
 grid on 
 bar(r(1,:),r(2,:))
 ylabel('[]')
 xlabel('time [s]')
 title('position')
 
 
 subplot(3,1,2)
 hold on 
 grid on 
 bar(r(1,:),r(3,:))
 ylabel('[/s]')
 xlabel('time [s]')
  title('velocity')
 
 subplot(3,1,3)
 hold on 
 grid on 
 bar(r(1,:),r(4,:))
 ylabel('[/s^2]')
 xlabel('time [s]')
  title('acceleration')
 end
 