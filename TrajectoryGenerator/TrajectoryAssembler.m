classdef TrajectoryAssembler < handle
    
properties   (SetAccess = public)
    
conditionsArray
trajectory

interp
eval

end
    
methods (Access = public)
    
    function obj = TrajectoryAssembler(conditionsArray)
        
        obj.conditionsArray=conditionsArray;
        obj.interp = Interpolator;
        obj.eval = Evaluator;
    
    end
    
    function obj = assemble(obj,frequency)
        
        
     for i=1:numel(obj.conditionsArray)
         
         obj.interp.findCoeffs(obj.conditionsArray(i).conditions);
         coeffs = obj.interp.getCoefficients();
         obj.eval.evaluate(coeffs,frequency,obj.conditionsArray(i).conditions);
         
         if i==1
         obj.trajectory = obj.eval.getTrajectory();
         else
         obj.trajectory=[obj.trajectory(:,1:end-1), obj.eval.getTrajectory()];
         end
         
         
         
     end
        
    end
    

    
end

end