classdef Interpolator < handle
    
    
    
   properties (SetAccess = private)
      
       coeffs
       
   end
   
   
   
     methods 
       
      
       function obj = findCoeffs(obj,conditions)
           
        obj.coeffs(1)=(12*conditions(1) - 12*conditions(4) - 6*conditions(2)*conditions(7) - 6*conditions(5)*conditions(7) + 6*conditions(2)*conditions(8) + 6*conditions(5)*conditions(8) + conditions(3)*conditions(7)^2 - conditions(6)*conditions(5)^2 + conditions(3)*conditions(8)^2 - conditions(6)*conditions(8)^2 - 2*conditions(3)*conditions(7)*conditions(8) + 2*conditions(6)*conditions(7)*conditions(8))/(2*(conditions(7) - conditions(8))^5);
        obj.coeffs(2)=-(30*conditions(1)*conditions(7) + 30*conditions(1)*conditions(8) - 30*conditions(4)*conditions(7) - 30*conditions(4)*conditions(8) - 14*conditions(2)*conditions(7)^2 - 16*conditions(5)*conditions(7)^2 + 2*conditions(3)*conditions(7)^3 - 3*conditions(6)*conditions(7)^3 + 16*conditions(2)*conditions(8)^2 + 14*conditions(5)*conditions(8)^2 + 3*conditions(3)*conditions(8)^3 - 2*conditions(6)*conditions(8)^3 - 4*conditions(3)*conditions(7)*conditions(8)^2 - conditions(3)*conditions(7)^2*conditions(8) + conditions(6)*conditions(7)*conditions(8)^2 + 4*conditions(6)*conditions(7)^2*conditions(8) - 2*conditions(2)*conditions(7)*conditions(8) + 2*conditions(5)*conditions(7)*conditions(8))/(2*(conditions(7) - conditions(8))^5);
        obj.coeffs(3)=(20*conditions(1)*conditions(7)^2 + 20*conditions(1)*conditions(8)^2 - 20*conditions(4)*conditions(7)^2 - 20*conditions(4)*conditions(8)^2 - 8*conditions(2)*conditions(7)^3 - 12*conditions(5)*conditions(7)^3 + conditions(3)*conditions(7)^4 - 3*conditions(6)*conditions(7)^4 + 12*conditions(2)*conditions(8)^3 + 8*conditions(5)*conditions(8)^3 + 3*conditions(3)*conditions(8)^4 - conditions(6)*conditions(8)^4 + 28*conditions(2)*conditions(7)*conditions(8)^2 - 32*conditions(2)*conditions(7)^2*conditions(8) + 32*conditions(5)*conditions(7)*conditions(8)^2 - 28*conditions(5)*conditions(7)^2*conditions(8) + 4*conditions(3)*conditions(7)^3*conditions(8) - 4*conditions(6)*conditions(7)*conditions(8)^3 - 8*conditions(3)*conditions(7)^2*conditions(8)^2 + 8*conditions(6)*conditions(7)^2*conditions(8)^2 + 80*conditions(1)*conditions(7)*conditions(8) - 80*conditions(4)*conditions(7)*conditions(8))/(2*(conditions(7) - conditions(8))^5);

         if conditions(7)==0
        
        obj.coeffs(4)=conditions(3)/2;
        obj.coeffs(5)=conditions(2);
        obj.coeffs(6)=conditions(1);
  
         else    
        obj.coeffs(4)=(conditions(2)*conditions(7)^5 - conditions(3)*conditions(8)^5 - 60*conditions(1)*conditions(7)*conditions(8)^2 - 60*conditions(1)*conditions(7)^2*conditions(8) + 60*conditions(4)*conditions(7)*conditions(8)^2 + 60*conditions(4)*conditions(7)^2*conditions(8) - 36*conditions(2)*conditions(7)*conditions(8)^3 + 24*conditions(2)*conditions(7)^3*conditions(8) - 24*conditions(5)*conditions(7)*conditions(8)^3 + 36*conditions(5)*conditions(7)^3*conditions(8) - 4*conditions(3)*conditions(7)*conditions(8)^4 - 3*conditions(3)*conditions(7)^4*conditions(8) + 3*conditions(6)*conditions(7)*conditions(8)^4 + 4*conditions(6)*conditions(7)^4*conditions(8) + 12*conditions(2)*conditions(7)^2*conditions(8)^2 - 12*conditions(5)*conditions(7)^2*conditions(8)^2 + 8*conditions(3)*conditions(7)^2*conditions(8)^3 - 8*conditions(6)*conditions(7)^3*conditions(8)^2)/(2*(conditions(7) - conditions(8))^5);
        obj.coeffs(5)=(2*conditions(5)*conditions(7)^5 - 2*conditions(2)*conditions(8)^5 + 10*conditions(2)*conditions(7)*conditions(8)^4 - 10*conditions(5)*conditions(7)^4*conditions(8) + 2*conditions(3)*conditions(7)*conditions(8)^5 - 2*conditions(6)*conditions(7)^5*conditions(8) + 60*conditions(1)*conditions(7)^2*conditions(8)^2 - 60*conditions(4)*conditions(7)^2*conditions(8)^2 + 16*conditions(2)*conditions(7)^2*conditions(8)^3 - 24*conditions(2)*conditions(7)^3*conditions(8)^2 + 24*conditions(5)*conditions(7)^2*conditions(8)^3 - 16*conditions(5)*conditions(7)^3*conditions(8)^2 - conditions(3)*conditions(7)^2*conditions(8)^4 - 4*conditions(3)*conditions(7)^3*conditions(8)^3 + 3*conditions(3)*conditions(7)^4*conditions(8)^2 - 3*conditions(6)*conditions(7)^2*conditions(8)^4 + 4*conditions(6)*conditions(7)^3*conditions(8)^3 + conditions(6)*conditions(7)^4*conditions(8)^2)/(2*(conditions(7) - conditions(8))^5);
        obj.coeffs(6)=-(2*conditions(1)*conditions(8)^5 - 2*conditions(4)*conditions(7)^5 - 10*conditions(1)*conditions(7)*conditions(8)^4 + 10*conditions(4)*conditions(7)^4*conditions(8) - 2*conditions(2)*conditions(7)*conditions(8)^5 + 2*conditions(5)*conditions(7)^5*conditions(8) + 20*conditions(1)*conditions(7)^2*conditions(8)^3 - 20*conditions(4)*conditions(7)^3*conditions(8)^2 + 10*conditions(2)*conditions(7)^2*conditions(8)^4 - 8*conditions(2)*conditions(7)^3*conditions(8)^3 + 8*conditions(5)*conditions(7)^3*conditions(8)^3 - 10*conditions(5)*conditions(7)^4*conditions(8)^2 + conditions(3)*conditions(7)^2*conditions(8)^5 - 2*conditions(3)*conditions(7)^3*conditions(8)^4 + conditions(3)*conditions(7)^4*conditions(8)^3 - conditions(6)*conditions(7)^3*conditions(8)^4 + 2*conditions(6)*conditions(7)^4*conditions(8)^3 - conditions(6)*conditions(7)^5*conditions(8)^2)/(2*(conditions(7) - conditions(8))^5);
         end
    
    
         end
         
         function coefficients = getCoefficients(obj)
        
          coefficients = [obj.coeffs];
        
         end
           
       end
      
      
end
   

      

   
   
