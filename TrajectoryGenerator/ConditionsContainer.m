classdef ConditionsContainer < handle
    
    % this class store 
    
   properties (SetAccess = private)
      
       conditions = zeros(1,8);
       
   end
   
   
   
     methods 
       
      
    function obj = setPositions(obj,initPos,endPos)
        
        obj.conditions(1)=initPos;
        obj.conditions(4)=endPos;
        
    end
    
    
    function obj = setVelocities(obj,initVel,endVel)
        
        obj.conditions(2)=initVel;
        obj.conditions(5)=endVel;
           
    end
    
    
    function obj = setAccelerations(obj,initAcc,endAcc)
        
        obj.conditions(3)=initAcc;
        obj.conditions(6)=endAcc;
           
    end
    
    
    function obj = setTimes(obj,initTime,endTime)
        
        obj.conditions(7)=initTime;
        obj.conditions(8)=endTime;
        
    end
    
    function conditions = getConditions(obj)
        
       conditions = [obj.conditions];
        
    end
     
    
    
     end
   

    
      
end