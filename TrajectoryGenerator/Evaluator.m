classdef Evaluator < handle
    
   properties 
      
     position
     velocity
     acceleration
     time
      
   end
   
   
   
     methods 
       
         
    function obj = evaluate(obj,coefficients,frequency,conditions)
        
   
        obj.time = conditions(7):(1/frequency):conditions(8);
   
        obj.position = polyval(coefficients,obj.time);
        obj.velocity = polyval(polyder(coefficients),obj.time);
        obj.acceleration = polyval(polyder(polyder(coefficients)),obj.time);
        
    end
    
    
    
    function trajectory = getTrajectory(obj)
        
        trajectory=[obj.time;obj.position;obj.velocity;obj.acceleration;];
        
    end
     
     end

      
end