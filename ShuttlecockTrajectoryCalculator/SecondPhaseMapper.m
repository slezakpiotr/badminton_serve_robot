classdef SecondPhaseMapper < handle
    
    properties
        
    solver;
    netLocation=5;
    simulationTime;
    resolution;
    
    map;
    
        
    end
    
    
    methods 
        
        function obj = SecondPhaseMapper(solver,simulationTime,resolution)
            
            obj.solver=solver;
            obj.simulationTime=simulationTime;
            obj.resolution=resolution;
            obj.map = SecondPhaseMap;
            
        end
        
         function obj = makeMap(obj,velocityScope,angleScope,x0,y0)
                
             
             obj.map.velocity = linspace(velocityScope(1),velocityScope(2),obj.resolution);
             obj.map.angle = linspace(angleScope(1),angleScope(2),obj.resolution);
             
             
                for i=1:numel(obj.map.velocity)
                        for j=1:numel(obj.map.angle)
                            
                                   obj.solver.getSolution(x0,y0,obj.map.velocity(i),obj.map.angle(j),obj.simulationTime);
                            
                                    obj.map.range(i,j)=obj.solver.data(end,3);
                                             
                                    if obj.map.range(i,j) < obj.netLocation
                                    obj.map.netHeight(i,j)=0;
                                    else
                                        in=find(obj.solver.data(:,3)>obj.netLocation);
                                       obj.map.netHeight(i,j)=obj.solver.data(min(in),4);
                                    end
                            
                            
                        end
                end
             
             

    
            
         end
        
     
        
        
        
    end
        
        
 
end