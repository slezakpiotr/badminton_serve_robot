classdef BallisticEquationSolver < handle
    
properties (SetAccess = private)
    
rho=1;                                                              
S=0.07;                                                                  
cd=0.3;                                                                
mass=0.045;                                                             
gravity=9.8065;                                                              
C=[-5.3125e-07;0.00013125;-0.0111625;0.5];            


time
data


end
    


methods (Access = private)
    
    
function dY=getEquation(obj,t,Y)


V=sqrt(Y(1)^2+Y(2)^2);


k=2*(obj.C(1)*V^3+obj.C(2)*V^2+obj.C(3)*V+obj.C(4));

Faero=0.5*obj.rho*obj.S*V^2*obj.cd;
Faero=Faero*k;

gamma=atan2(Y(2),Y(1));

Faereo_x=cos(gamma)*Faero;
Faereo_y=sin(gamma)*Faero;

Fgrav=obj.mass*obj.gravity;

dY(1)=-Faereo_x/obj.mass;                 
dY(2)=-(Faereo_y*sign(Y(2))+Fgrav)/obj.mass;
dY(3)=Y(1);
dY(4)=Y(2);
dY=dY';


end

end

methods (Access = public)

function obj = getSolution(obj,x0,y0,v,alfa,simulatonTime)



[t1, Y]=ode45(@obj.getEquation,[0 simulatonTime],[(v)*cosd(alfa) (v)*sind(alfa) x0 y0]');
 
    x=linspace(min(Y(:,3)),max(Y(:,3)),200);
    obj.data=zeros(200,3);
    
    for jj=1:4
    obj.data(:,jj)=interp1(Y(:,3),Y(:,jj),x,'linear');
    end
    obj.time=interp1(Y(:,3),t1,x,'linear');
    
    pos_ground=find(obj.data(:,4)<0,1,'first');
    obj.data(pos_ground:end,:)=[];
        
    
end

end


end
    
