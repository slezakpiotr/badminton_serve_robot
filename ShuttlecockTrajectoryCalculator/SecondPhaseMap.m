classdef SecondPhaseMap
    
properties (SetAccess = public)
    
    velocity
    angle
    
    range
    netHeight
    
    
end



methods
    
    function drawRangeMap(obj)
    
    surf(obj.angle,obj.velocity,obj.range)    
        
    end
    
    
    function drawHeightMap(obj)
    

    surf(obj.angle,obj.velocity,obj.netHeight)    

        
    end
    
    function saveMap(obj,name)
    
        v=obj.velocity;
        a=obj.angle;
        range=obj.range;
        neth=obj.netHeight;
        
        
    save(name,'v','a','range','neth');

        
    end
    
    
end
    
    
    
end
