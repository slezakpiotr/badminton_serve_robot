close all


solver = BallisticEquationSolver;
simulationTime=5;
resolution=50;

velocityScope = [1 40];
angleScope = [20 80];

x0=0;
y0=0;


mapper = SecondPhaseMapper(solver,simulationTime,resolution);
mapper.makeMap(velocityScope,angleScope,x0,y0);


figure(1)

mapper.map.drawRangeMap

grid on 
 
ylabel('velocity')
xlabel('angle')
zlabel('distance')

view(-50,50)

title('range map')

figure(2)

mapper.map.drawHeightMap

grid on 
 
ylabel('velocity')
xlabel('angle')
zlabel('over net')

view(-50,50)

title('height map')

mapper.map.saveMap('plik.mat')


