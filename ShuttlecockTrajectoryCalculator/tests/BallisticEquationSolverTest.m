close all

visualization = 1;


solver = BallisticEquationSolver;


x0=0;
y0=0;
v=20;
alfa=45;


solver.getSolution(x0,y0,v,alfa,5);


if visualization == 1

figure(1)
hold on 
grid on 

axis equal
plot(solver.data(:,3),solver.data(:,4))

end